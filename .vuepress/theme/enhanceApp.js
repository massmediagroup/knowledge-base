import Auth from '@okta/okta-vue';
import {oktaConfig} from './oktaConfig';
import AuthCallback from "./components/AuthCallback";
export default ({
                    Vue, // the version of Vue being used in the VuePress app
                    options, // the options for the root Vue instance
                    router, // the router instance for the app
                    siteData // site metadata
                }) => {
    Vue.use(Auth, oktaConfig);
    Vue.component('AuthCallback', AuthCallback)
};