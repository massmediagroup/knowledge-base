module.exports = {
    title: "My Documentation Site",
    description: "This is going to be awesome!",
    themeConfig: {
        codeLanguages: {
            php: 'PHP',
            twig: 'Twig',
        },
        nav: [
            { text: "Home", link: "/" },
            { text: "Docs", link: "/docs/" }
        ],
        sidebar: [
            {
                title: 'Group 1',   // required
                path: '/',      // optional, which should be a absolute path.
                children: [
                    '/'
                ]
            },
            {
                title: 'About docs',
                path: '/docs/',
                children: [
                    '/docs/HowToAddNewPage'
                ]
            }
        ],
        sidebarDepth: 3
    },
    theme: 'craftdocs',
    markdown: {
        anchor: { level: [2, 3] },
        config(md) {
            let markup = require('vuepress-theme-craftdocs/markup')
            md.use(markup)
        }
    }
};