export const oktaConfig = {
    issuer: "",
    client_id: "",
    redirect_uri: "http://localhost:8080/implicit/callback/",
    scope: "openid profile email"
};